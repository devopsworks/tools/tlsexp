package logger

import (
	"time"

	"gitlab.com/devopsworks/tools/tlsexp/target"

	log "github.com/sirupsen/logrus"
)

// Config is a void struct here
type Config struct{}

// Init is fake here since we need no init
func (s *Config) Init() {
	log.Debug("logger initialized")
}

// Finalize log printing
func (s *Config) Finalize() {

}

// Log a point to stderr
func (s *Config) Log(en target.Entry, ex target.Expiration) {
	rc := "ok"
	if ex.Error != nil {
		rc = ex.Error.Error()
	}

	left := float64(ex.Expires.Sub(time.Now().UTC()) / time.Hour)
	if left < 0 {
		left = 0
	}

	log.WithFields(log.Fields{
		"requested":  en.Requested,
		"hostname":   ex.Hostname,
		"ip":         ex.IP,
		"port":       en.Port,
		"protocol":   en.Protocol,
		"issuer":     ex.Issuer,
		"expires":    ex.Expires,
		"hours_left": left,
		"status":     rc,
	}).Infof("expiration")
}
