package influxdb

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"time"

	"gitlab.com/devopsworks/tools/tlsexp/target"

	log "github.com/sirupsen/logrus"
)

// Config information
type Config struct {
	DB          string
	URL         string
	User        string
	Pass        string
	Retries     int
	Timeout     time.Duration
	Measurement string
	Tags        string
}

// Init is fake here since we need no init
func (s Config) Init() {
	if s.URL == "" || s.Measurement == "" {
		log.Debug("influxdb not activated (influxurl or influxmeasurement not set)")
		return
	}

	log.Debugf("influxdb initialized for %s", s.URL)
}

// Finalize printing
func (s *Config) Finalize() {

}

// Log a point to a server
func (s *Config) Log(en target.Entry, ex target.Expiration) {
	// Null-Object type bypass if there is no server information
	if s.DB == "" || s.URL == "" {
		return
	}

	go s.doLog(en, ex)
}

func (s *Config) doLog(en target.Entry, ex target.Expiration) {
	host, err := os.Hostname()
	if err != nil {
		host = "unknown"
	}

	influxString := fmt.Sprintf("%s,requested_host=%s,checked_host=%s,host=%s", s.Measurement, en.Requested, ex.Hostname, host)
	if s.Tags != "" {
		influxString = fmt.Sprintf("%s,%s", influxString, s.Tags)
	}

	// set status to 1 if an error occurred during check
	rc := 0
	if ex.Error != nil {
		rc = 1
	}
	influxString = fmt.Sprintf("%s remaining_hours=%.2f,status=%d", influxString,
		float64(ex.Expires.Sub(time.Now().UTC())/time.Hour), rc)

	buf := bytes.NewBufferString(influxString)

	var uri string
	if s.URL[len(s.URL)-1] == '/' {
		uri = fmt.Sprintf("%swrite?db=%s", s.URL, s.DB)
	} else {
		uri = fmt.Sprintf("%s/write?db=%s", s.URL, s.DB)
	}

	if s.User != "" {
		uri += fmt.Sprintf("&u=%s&p=%s", url.PathEscape(s.User), url.PathEscape(s.Pass))
	}

	client := &http.Client{
		Timeout: s.Timeout,
	}

	var resp *http.Response

	for try := 0; try < s.Retries; try++ {
		resp, err = client.Post(uri, "application/x-www-form-urlencoded", buf)
		if err == nil {
			log.Info("done")

			break
		}
	}
	if err != nil {
		log.Errorf("error writing %s to influx after %d tries (timeout: %s): %v", buf, s.Retries, s.Timeout, err)
		return
	}

	if resp.StatusCode != 204 {
		log.Errorf("unable to write to influxdb server %s, got response: %s", s.URL, resp.Status)
	}
}
