package influxdb

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/devopsworks/tools/tlsexp/target"
)

func TestLog(t *testing.T) {
	testCases := []struct {
		db          string
		user        string
		pass        string
		measurement string
		tags        string
		addSlash    bool
		withError   bool
	}{
		{db: "foo1"},
		{db: "foo2", user: "u"},
		{db: "foo3", user: "u", pass: "p"},
		{db: "foo4", user: "u", pass: "p", measurement: "m"},
		{db: "foo5", user: "u", pass: "p", measurement: "m", tags: "t1"},
		{db: "foo6", user: "u", pass: "p", measurement: "m", tags: "t1,t2"},
		{db: "foo1s", addSlash: true},
		{db: "foo2s", user: "u", addSlash: true},
		{db: "foo3s", user: "u", pass: "p", addSlash: true},
		{db: "foo4s", user: "u", pass: "p", measurement: "m", addSlash: true},
		{db: "foo5s", user: "u", pass: "p", measurement: "m", tags: "t1", addSlash: true},
		{db: "foo6s", user: "u", pass: "p", measurement: "m", tags: "t1,t2", addSlash: true},
		{db: "foo1e", withError: true},
		{db: "foo2e", user: "u", withError: true},
		{db: "foo3e", user: "u", pass: "p", withError: true},
		{db: "foo4e", user: "u", pass: "p", measurement: "m", withError: true},
		{db: "foo5e", user: "u", pass: "p", measurement: "m", tags: "t1", withError: true},
		{db: "foo6e", user: "u", pass: "p", measurement: "m", tags: "t1,t2", withError: true},
	}

	// Start a local HTTP server
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		// Test request parameters
		// equals(t, req.URL.String(), "/some/path")
		// Send response to be tested
		_, _ = rw.Write([]byte(`OK`))
	}))
	// Close the server when
	defer server.Close()

	for _, tc := range testCases {
		t.Run(tc.db, func(t *testing.T) {
			u := server.URL
			if tc.addSlash {
				u += "/"
			}
			c := &Config{
				DB:          tc.db,
				URL:         u,
				User:        tc.user,
				Pass:        tc.pass,
				Retries:     2,
				Timeout:     1000 * time.Second,
				Measurement: tc.measurement,
				Tags:        tc.tags,
			}

			c.Init()

			c.Log(
				target.Entry{
					Requested: "host",
					Protocol:  "imap",
					Port:      "",
				},
				target.Expiration{
					Hostname: "host",
					Expires:  time.Now(),
					Error:    nil,
				})
			// Use Client & URL from our local test server
			// api := API{server.Client(), server.URL}
			// body, err := api.DoStuff()

			// ok(t, err)
			// equals(t, []byte("OK"), body)

		})
	}
}
