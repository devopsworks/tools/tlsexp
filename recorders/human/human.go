package human

import (
	"fmt"
	"os"
	"sort"

	// "text/tabwriter"
	"time"

	"github.com/olekukonko/tablewriter"
	"gitlab.com/devopsworks/tools/tlsexp/target"

	log "github.com/sirupsen/logrus"
)

// Config is a void struct here
type Config struct {
	output [][]string
}

var (
	// info     = teal
	warn     = yellow
	critical = red
)

var (
	// black   = color("\033[1;30m%s\033[0m")
	red = color("\033[1;31m%s\033[0m")
	// green   = color("\033[1;32m%s\033[0m")
	yellow = color("\033[1;33m%s\033[0m")
	// purple  = color("\033[1;34m%s\033[0m")
	// magenta = color("\033[1;35m%s\033[0m")
	teal = color("\033[1;36m%s\033[0m")
	// white   = color("\033[1;37m%s\033[0m")
)

func color(colorString string) func(...interface{}) string {
	sprint := func(args ...interface{}) string {
		return fmt.Sprintf(colorString,
			fmt.Sprint(args...))
	}
	return sprint
}

// Init is fake here since we need no init
func (s *Config) Init() {
	log.Debug("human output initialized")
}

// Finalize printing
func (s *Config) Finalize() {
	// s.Finalize2()

	// s.sortByHours()
	sort.Slice(s.output, func(i, j int) bool {
		return s.output[i][4] < s.output[j][4]
	})

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"URL", "address", "port", "proto", "issuer", "expires", "hours"})
	table.SetBorders(tablewriter.Border{Left: true, Top: false, Right: true, Bottom: false})
	table.SetCenterSeparator("|")
	table.SetColumnAlignment([]int{
		tablewriter.ALIGN_LEFT, tablewriter.ALIGN_CENTER, tablewriter.ALIGN_RIGHT,
		tablewriter.ALIGN_LEFT, tablewriter.ALIGN_LEFT, tablewriter.ALIGN_LEFT, tablewriter.ALIGN_RIGHT,
	})
	table.SetAutoWrapText(false)

	table.SetAutoMergeCellsByColumnIndex([]int{0, 2, 3, 4, 5, 6})
	table.SetRowLine(true)

	table.AppendBulk(s.output) // Add Bulk Data
	table.Render()

	s.output = [][]string{}
	// for _, o := range s.output {
	// 	fmt.Println(o)
	// }
}

// Log a point to stderr
func (s *Config) Log(en target.Entry, ex target.Expiration) {
	left := float64(ex.Expires.Sub(time.Now().UTC()) / time.Hour)
	if left < 0 {
		left = 0
	}

	exp := teal
	dns := teal
	col := teal

	switch {
	case left < 120:
		exp = critical
	case left < 240:
		exp = warn
	}

	if ex.SNI != en.Requested {
		dns = warn
	}

	if ex.Error != nil {
		dns = critical
	}

	s.output = append(s.output, []string{
		dns(en.Requested),
		col(ex.IP),
		col(en.Port),
		col(en.Protocol),
		col(ex.Issuer),
		exp(ex.Expires.Format("2006-01-02 15:04")),
		exp(fmt.Sprintf("%.2f", left)),
	})
}

// func (s *Config) sortByHours() {
// 	sorted := [][]string{}
// 	for _, entry := range s.output {

// 	}
// }
