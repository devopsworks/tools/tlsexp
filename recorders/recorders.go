package recorders

import "gitlab.com/devopsworks/tools/tlsexp/target"

type Recorder interface {
	Init()
	Finalize()
	Log(target.Entry, target.Expiration)
}
