package yaml

import (

	// "text/tabwriter"

	"fmt"

	"gitlab.com/devopsworks/tools/tlsexp/target"
	yml "gopkg.in/yaml.v2"

	log "github.com/sirupsen/logrus"
)

// Config is a void struct here
type Config struct {
	entries map[string]target.Entry
}

// Init is fake here since we need no init
func (s *Config) Init() {
	log.Debug("JSON output initialized")
}

// Finalize printing
func (s *Config) Finalize() {
	b, err := yml.Marshal(s.entries)
	if err != nil {
		log.Errorf("error marshalling yaml: %s", err)
		return
	}
	fmt.Println(string(b))
}

// Log a point to stderr
func (s *Config) Log(en target.Entry, ex target.Expiration) {
	if s.entries == nil {
		s.entries = make(map[string]target.Entry)
	}

	s.entries[ex.Hostname] = en
}
