package main

import (
	"fmt"
	"net/url"
	"os"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/namsral/flag"

	"gitlab.com/devopsworks/tools/tlsexp/checkers/generic"
	"gitlab.com/devopsworks/tools/tlsexp/checkers/starttls"
	"gitlab.com/devopsworks/tools/tlsexp/checkers/starttlsmx"
	"gitlab.com/devopsworks/tools/tlsexp/target"

	"gitlab.com/devopsworks/tools/tlsexp/recorders"
	mhum "gitlab.com/devopsworks/tools/tlsexp/recorders/human"
	minf "gitlab.com/devopsworks/tools/tlsexp/recorders/influxdb"
	mjson "gitlab.com/devopsworks/tools/tlsexp/recorders/json"
	mlog "gitlab.com/devopsworks/tools/tlsexp/recorders/logger"
	mprom "gitlab.com/devopsworks/tools/tlsexp/recorders/prometheus"
	myaml "gitlab.com/devopsworks/tools/tlsexp/recorders/yaml"
)

//go:generate go run golang.org/x/lint/golint@v0.0.0-20210508222113-6edffad5e616 -set_exit_status main.go
//go:generate go run github.com/golangci/golangci-lint/cmd/golangci-lint@v1.57.1 run

// Version from git sha1/tags
var Version string

// Checker is an interface to expiration date checker
type Checker interface {
	Check(chan target.Entry, *sync.WaitGroup)
}

// Measurer defines what metrics handlers should implement
// type Measurer interface {
// 	Init()
// 	Log(string, string, string, target.Expiration) // requested hostname, protocol, target.Expiration
// 	Finalize()                                     // finalize log printing
// }

func main() {
	fs := flag.NewFlagSetWithEnvPrefix(os.Args[0], "TLSEXP", 0)

	var influxServer = fs.String("influxserver", "", "influxdb server URL (no events are send if not set)")
	var influxDB = fs.String("influxdb", "", "influxdb database (no events are send if not set)")
	var influxUser = fs.String("influxuser", "", "influxdb username (default: none)")
	var influxPass = fs.String("influxpass", "", "influxdb password (default: none)")
	var influxMeasurement = fs.String("influxmeasurement", "", "influxdb measurement (default: none, required when server is set)")
	var influxTags = fs.String("influxtags", "", "comma-separated k=v pairs of influxdb tags (default: none, example: 'foo=bar,fizz=buzz')")
	var influxRetry = fs.Int("influxretry", 3, "how many times we try to send the event to influxdb (default: 3)")
	var influxTimeout = fs.Int("influxtimeout", 2000, "how many milliseconds do we allow influxdb POST to take (default: 2000)")
	var version = fs.Bool("version", false, "show version")
	var delay = fs.Int("delay", 0, "delay between checks in seconds (min: 10)")
	var promPort = fs.Int("promport", 0, "export prometheus metrics on this port (disabled if not present)")
	var promBind = fs.String("prombind", "0.0.0.0", "export prometheus port on this interface (default: 0.0.0.0)")
	var level = fs.String("loglevel", "info", "debug level")
	var format = fs.String("f", "human", "log format (human, json, yaml, log)")
	var urls = fs.String("urls", "", "coma-separated list of URLs to check (or pass them as arguments)")

	err := fs.Parse(os.Args[1:])
	if err != nil {
		fmt.Fprintf(os.Stderr, "unable to parse flags: %v", err)
		os.Exit(1)
	}

	if *version {
		fmt.Fprintf(os.Stderr, "version %s\n", Version)
		os.Exit(0)
	}

	if *delay < 0 {
		fmt.Fprintf(os.Stderr, "error: delay %d is too short (minimum is 10 seconds)", *delay)
		os.Exit(1)
	}

	if *promPort > 65535 || *promPort < 0 {
		fmt.Fprintf(os.Stderr, "error: HTTP prometheus port %d is invalid", *promPort)
		os.Exit(1)
	}

	// Setup logging
	l, err := log.ParseLevel(*level)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: unable to understand requested logging level %s: %v\n", *level, err)
		os.Exit(1)
	}
	log.SetLevel(l)

	if *format == "json" {
		log.SetFormatter(&log.JSONFormatter{})
	}

	// Create & initialize checkers
	aggregated := []string{}

	// Aggregate leftover args
	for _, u := range fs.Args() {
		u = strings.TrimSpace(u)
		if u != "" {
			aggregated = append(aggregated, u)
		}
	}

	// Aggregate urls passed via -url or by URL environment var
	for _, u := range strings.Split(*urls, ",") {
		u = strings.TrimSpace(u)
		if u != "" {
			aggregated = append(aggregated, u)
		}
	}

	// Check that there are any leftover arguments or -urls is supplied
	if len(aggregated) == 0 {
		fmt.Fprintln(os.Stderr, "error: no URLs supplied (either use -url or add them at the end of options")
		os.Exit(1)
	}

	// Create measurers
	measurers := []recorders.Recorder{}

	if *promPort != 0 {
		measurers = append(measurers, &mprom.Config{
			Bind: *promBind,
			Port: *promPort,
		})
	}

	if *influxServer != "" {
		measurers = append(measurers, &minf.Config{
			URL:         *influxServer,
			DB:          *influxDB,
			User:        *influxUser,
			Pass:        *influxPass,
			Measurement: *influxMeasurement,
			Tags:        *influxTags,
			Retries:     *influxRetry,
			Timeout:     time.Duration(*influxTimeout) * time.Second,
		})
	}

	switch *format {
	case "human":
		measurers = append(measurers, &mhum.Config{})
	case "json":
		measurers = append(measurers, &mjson.Config{})
	case "yaml":
		measurers = append(measurers, &myaml.Config{})
	default:
		measurers = append(measurers, &mlog.Config{})
	}

	// Initialize measurers
	for _, m := range measurers {
		m.Init()
	}

	realMain(measurers, aggregated, *delay)
}

func realMain(measurers []recorders.Recorder, aggregated []string, delay int) {
	checkers := getCheckers(aggregated)

	for {
		run(checkers, measurers)
		if delay == 0 {
			break
		}

		time.Sleep(time.Duration(delay) * time.Second)
	}
	os.Exit(0)
}

func run(checkers []Checker, measurers []recorders.Recorder) {
	var wg sync.WaitGroup
	entries := make(chan target.Entry, 500)

	log.Debug("starting checkers goroutines")

	for _, c := range checkers {
		wg.Add(1)
		go c.Check(entries, &wg)
	}

	log.Debug("waiting for checkers")

	wg.Wait()

	close(entries)

	for res := range entries {
		for _, e := range res.Expirations {
			for _, m := range measurers {
				m.Log(res, e)
			}
		}
	}

	for _, m := range measurers {
		m.Finalize()
	}
}

func getCheckers(urls []string) []Checker {
	var checkers []Checker

	// Handle leftover arguments
	for _, a := range urls {
		log.Debugf("parsing URL %s", a)
		u, err := url.Parse(a)
		if err != nil {
			log.Errorf("unable to parse URL from %s: %v", a, err)
			continue
		}
		if u.Opaque != "" {
			log.Errorf("opaque data from URL %s; is URL valid ?", a)
			continue
		}
		// switch parts[0] {
		switch u.Scheme {
		case "starttlsmx", "smtpmx":
			checkers = append(checkers, &starttlsmx.Checker{
				Hostname: u.Hostname(),
				Protocol: u.Scheme,
			})
		case "submission":
			checkers = append(checkers, &starttls.Checker{
				Hostname: u.Hostname(),
				Protocol: u.Scheme,
			})
		case "starttls", "smtp":
			checkers = append(checkers, &starttls.Checker{
				Hostname: u.Hostname(),
				Protocol: u.Scheme,
			})
		default:
			if u.Port() != "" {
				log.Debugf("using explicit port %s", u.Port())
				checkers = append(checkers, &generic.Checker{
					Hostname: u.Hostname(),
					Protocol: u.Scheme,
					Port:     u.Port(),
				})
			} else if val, ok := generic.Services[u.Scheme]; ok {
				checkers = append(checkers, &generic.Checker{
					Hostname: u.Hostname(),
					Protocol: u.Scheme,
					Port:     val,
				})
			} else if u.Scheme == "" {
				checkers = append(checkers, &generic.Checker{
					Hostname: a,
					Protocol: "https",
					Port:     "443",
				})
			} else {
				log.Fatalf("service '%s' is not supported in '%s'\n", u.Scheme, a)
			}
		}
	}

	return checkers
}
