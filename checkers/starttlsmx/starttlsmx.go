package starttlsmx

import (
	"net"
	"sync"

	log "github.com/sirupsen/logrus"

	"github.com/pkg/errors"
	"gitlab.com/devopsworks/tools/tlsexp/checkers/starttls"
	"gitlab.com/devopsworks/tools/tlsexp/target"
)

// Checker type handles TLS checking
type Checker struct {
	Hostname    string
	Protocol    string
	Port        string
	SubCheckers []*starttls.Checker
}

// Check gets expiration dates for starttls smtp servers
func (c *Checker) Check(e chan target.Entry, wg *sync.WaitGroup) {
	defer wg.Done()

	tg := target.Entry{
		Requested: c.Hostname,
		Protocol:  c.Protocol,
		Port:      c.Port,
	}

	mx, err := net.LookupMX(c.Hostname)

	if err != nil {
		tg.Expirations = append(tg.Expirations,
			target.Expiration{
				Hostname: c.Hostname,
				Error:    errors.Wrap(err, "unable to do MX lookup"),
			})
		e <- tg
	}

	for _, m := range mx {
		c := starttls.Checker{
			Hostname: m.Host,
			Protocol: "starttls",
			Port:     "25",
		}
		log.Debugf("create STARTTLS subchecker for %s", c.Hostname)
		wg.Add(1)
		go c.Check(e, wg)
		// tg.Expirations = append(tg.Expirations, t.Expirations...)
	}

	e <- tg
}
