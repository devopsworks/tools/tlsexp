package starttls

import (
	"crypto/tls"
	"fmt"
	"net/smtp"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/devopsworks/tools/tlsexp/target"
)

// Checker type handles TLS checking
type Checker struct {
	Hostname string
	Protocol string
	Port     string
}

// Check gets expiration dates for starttls smtp servers
func (c *Checker) Check(e chan target.Entry, wg *sync.WaitGroup) {
	defer wg.Done()

	tg := target.Entry{
		Requested: c.Hostname,
		Protocol:  c.Protocol,
		Port:      c.Port,
	}

	tg.Port = "25"
	if c.Protocol == "submission" {
		tg.Port = "587"
	}

	tg.Expirations = append(tg.Expirations, target.Expiration{Hostname: c.Hostname})

	for i, hst := range tg.Expirations {
		t, issuer, err := getNotAfter(hst.Hostname, tg.Port)
		if err != nil {
			tg.Expirations[i].Error = err
			continue
		}
		tg.Expirations[i].Issuer = issuer
		tg.Expirations[i].Expires = t
	}

	e <- tg
}

func getNotAfter(srv, port string) (time.Time, string, error) {
	full := srv + ":" + port
	log.Debugf("checking starttls on %s", full)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         srv,
	}

	c, err := smtp.Dial(full)
	if err != nil {
		return time.Unix(0, 0), "connect timeout", err
	}

	defer c.Close()

	err = c.Hello("tlsexp.example.org")
	if err != nil {
		log.Debugf("hello error %v", err)
		return time.Unix(0, 0), "hello error", err
	}

	err = c.StartTLS(tlsconfig)
	if err != nil {
		log.Debugf("TLS error %v", err)
		return time.Time{}, "TLS error", err
	}

	st, ok := c.TLSConnectionState()
	if !ok {
		log.Debugf("unable to get TLSConnectionState")
		return time.Unix(0, 0), "", fmt.Errorf("unable to get TLSConnectionState")
	}

	// Set expiration in 20 years to start
	nextExp := time.Now().Add(time.Hour * 24 * 365 * 20)

	for _, cert := range st.PeerCertificates {
		if cert.NotAfter.Before(nextExp) {
			nextExp = cert.NotAfter
		}
		log.Debugf("PeerCertificate %s\n", cert.Subject.CommonName)
		log.Debugf("NotBefore %s\n", cert.NotBefore)
		log.Debugf("NotAfter %s\n", cert.NotAfter)
	}

	issuerName := fmt.Sprintf("%s/%s", st.PeerCertificates[0].Issuer.CommonName, st.PeerCertificates[0].Issuer.Organization[0])

	return nextExp, issuerName, nil
}
