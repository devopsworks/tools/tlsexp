package generic

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net"
	"strings"
	"sync"
	"time"

	"gitlab.com/devopsworks/tools/tlsexp/target"

	log "github.com/sirupsen/logrus"
)

// Checker type handles TLS checking
type Checker struct {
	Hostname string
	Protocol string
	Port     string
	// certificates []*x509.Certificate
}

// Services contains a map of supported generic services
var Services = map[string]string{
	"https":       "443",
	"http":        "443", // convenience helper
	"smtps":       "465",
	"submissions": "465",
	"imaps":       "993",
	"pop3s":       "995",
}

// Check gets expiration dates for starttls smtp servers
func (c *Checker) Check(e chan target.Entry, wg *sync.WaitGroup) {
	defer wg.Done()

	tg := target.Entry{
		Requested: c.Hostname,
		Protocol:  c.Protocol,
		Port:      c.Port,
	}

	entries, err := net.LookupHost(c.Hostname)
	if err != nil {
		tg.Expirations = append(tg.Expirations, target.Expiration{
			Hostname: c.Hostname,
			Error:    fmt.Errorf("unable to lookup host: %w", err),
		})
		e <- tg
		return
	}

	for i, m := range entries {
		if strings.Contains(m, ":") {
			m = fmt.Sprintf("[%s]", m)
		}

		exp, err := c.verifyCertificate(m, c.Hostname, c.Port)
		if err != nil {
			log.Warnf("error fetching certificates for %s: %v", c.Hostname, err)
			log.Debugf("setting error for target %d to %v", i, err)
			exp.Error = err
		}
		tg.Expirations = append(tg.Expirations, exp)
	}

	// for i, hst := range tg.Expirations {
	// 	t, issuer, err := c.verifyCertificate(hst.Hostname, c.Hostname, c.Port)

	// 	if err != nil {
	// 		log.Warnf("error fetching certificates for %s: %v", c.Hostname, err)
	// 		log.Debugf("setting error for target %d to %v", i, err)
	// 		tg.Expirations[i].Error = err
	// 	}

	// 	tg.Expirations[i].Expires = t
	// 	tg.Expirations[i].Issuer = issuer
	// }

	e <- tg
}

func (c *Checker) verifyCertificate(ip, srv, port string) (target.Expiration, error) {
	exp := target.Expiration{
		Hostname: srv,
		IP:       ip,
		Expires:  time.Time{},
	}

	full := fmt.Sprintf("%s:%s", ip, port)
	log.Debugf("fetching TLS certificates on %s", full)

	syspool, err := x509.SystemCertPool()
	if err != nil {
		exp.Error = fmt.Errorf("unable to get system CA pool: %w", err)
		return exp, exp.Error
	}

	// TLS config
	tlsconfig := &tls.Config{
		ServerName:         srv,
		RootCAs:            syspool,
		InsecureSkipVerify: true,
	}

	conn, err := net.DialTimeout("tcp", full, 10*time.Second)
	if err != nil {
		exp.Error = fmt.Errorf("unable to connect to host %s: %w", full, err)
		return exp, exp.Error
	}

	defer conn.Close()

	tlsConn := tls.Client(conn, tlsconfig)
	defer tlsConn.Close()

	err = tlsConn.Handshake()
	if err != nil {
		exp.Error = fmt.Errorf("unable to TLS handshake with host %s: %w", full, err)
		return exp, exp.Error
	}

	log.Debugf("issuer %s", tlsConn.ConnectionState().PeerCertificates[0].Issuer)

	// Get certificate issuer name
	issuerName := fmt.Sprintf("%s/%s",
		tlsConn.ConnectionState().PeerCertificates[0].Issuer.CommonName,
		tlsConn.ConnectionState().PeerCertificates[0].Issuer.Organization[0],
	)

	// We call tlsConn.VerifyHostname(srv) explicitely without relying on
	// tlsConn.VerifyHostname(srv) since errors.New("tls: handshake did not
	// verify certificate chain") will always be returned when
	// InsecureSkipVerify: true, is used
	exp.DNSNames = tlsConn.ConnectionState().PeerCertificates[0].DNSNames
	exp.Expires = tlsConn.ConnectionState().PeerCertificates[0].NotAfter
	exp.Issuer = issuerName
	exp.SNI = tlsConn.ConnectionState().ServerName
	exp.Error = tlsConn.ConnectionState().PeerCertificates[0].VerifyHostname(srv)

	return exp, nil
}
