# tlsexp

TLS certificate expiration checker.

Works for any TLS based service (HTTPS, IMAPS, SMTPS, ...) and for TLS-upgraded
connections (STARTTLS for now).

Results are exposed to Prometheus or sent to InfluxDB (InfluxDB is in the
roadmap, not ready yet).

By default, a single run is made and results are output to stdout.

Certificates that expires in less than 120 hours will be printed in red.
Certificates that expires in less than 240 hours will be printed in yellow.

## Usage

`tlsexp [options] <URL> [URL...]`

For instance:

```bash
$ tlsexp https://google.com starttlsmx://gmail.com submission://pro1.mail.ovh.net

|               URL                |             ADDRESS              | PORT |   PROTO    |     EXPIRES      |  HOURS   |
|----------------------------------|----------------------------------|------|------------|------------------|----------|
| alt2.gmail-smtp-in.l.google.com. | alt2.gmail-smtp-in.l.google.com. |      | starttls   | 2020-06-30 09:47 |  1308.00 |
|----------------------------------|----------------------------------|------|------------|------------------|----------|
| alt4.gmail-smtp-in.l.google.com. | alt4.gmail-smtp-in.l.google.com. |      | starttls   | 2020-06-30 09:47 |  1308.00 |
|----------------------------------|----------------------------------|------|------------|------------------|----------|
| alt3.gmail-smtp-in.l.google.com. | alt3.gmail-smtp-in.l.google.com. |      | starttls   | 2020-07-08 20:24 |  1511.00 |
|----------------------------------|----------------------------------|------|------------|------------------|----------|
| gmail-smtp-in.l.google.com.      |   gmail-smtp-in.l.google.com.    |      | starttls   | 2020-07-08 20:24 |  1511.00 |
|----------------------------------|----------------------------------|------|------------|------------------|----------|
| alt1.gmail-smtp-in.l.google.com. | alt1.gmail-smtp-in.l.google.com. |      | starttls   | 2020-07-08 20:24 |  1511.00 |
|----------------------------------|----------------------------------|------|------------|------------------|----------|
| pro1.mail.ovh.net                |        pro1.mail.ovh.net         |      | submission | 2020-11-24 23:59 |  4851.00 |
|----------------------------------|----------------------------------|------|------------|------------------|----------|
| google.com                       |    [2a00:1450:4007:805::200e]    |  443 | https      | 2030-01-01 00:00 | 84627.00 |
|                                  |----------------------------------|------|------------|------------------|----------|
|                                  |          172.217.18.206          |  443 | https      | 2030-01-01 00:00 | 84627.00 |
|----------------------------------|----------------------------------|------|------------|------------------|----------|
```

Options list:

- `delay <int>`: delay between checks in seconds (default: run only once,
  minimum: 10)
- `loglevel <string>`: debug level (default "warning")
- `logformat`: logging format ("json" or "text" for default)
- `promport <int>`: export prometheus metrics on this port (disabled if not
  present)
- `prombind <int>`: listen to prometheus port on this interface (default:
  0.0.0.0)
- `influxserver <string>`: influxdb server URL (no events are send if not set)
- `influxdb <string>`: influxdb database (no events are send if not set)
- `influxuser <string>`: influxdb username (default: none)
- `influxpass <string>`: influxdb password (default: none)
- `influxmeasurement <string>`: influxdb measurement (default: none, required
  when server is set)
- `influxtags <string>`: comma-separated k=v pairs of influxdb tags (default:
  none, example: 'foo=bar,fizz=buzz')
- `influxretry <int>`: how many times we try to send the event to influxdb
  (default: 3)
- `influxtimeout <int>`: how many milliseconds do we allow influxdb POST to
  take (default: 2000)
- `urls <string[,string...]>`: list of URL to check
- `version`: show version

Note that each option has a corresponding environment variable, prefixed by
`TLSEXP_`. For instance, `TLSEXP_URLS` is equivalent to the option `-urls`.

However, options at the CLI have higher precedence than environment variables.

Also note that you can can provide URL in several ways:

- using `TLSEXP_URLS`
- using `-urls`
- using CLI arguments after options

URLs provided as CLI arguments will always be used and *merged* to whatever
value is passed using `TLSEXP_URLS` or `-urls`.

## Install

### From docker

```bash
docker build . -t tlsexp
docker run tlsexp -logformat json -delay 100 https://google.com
```

### From source

```bash
git clone https://gitlab.com/devopsworks/tools/tlsexp/
cd tlsexp
make
```

Binary is built in `./bin/`. Use `make darwin` or `make windows` if you are
using weird OSes.

### From binary

Head to the [Releases page](https://gitlab.com/devopsworks/tools/tlsexp/-/releases) and download tarball.

## Supported protocols

- **smtp**,**starttls**: port **25** with in-protocol TLS upgrade
- **smtpmx**,**starttlsmx**: port **25** with MX lookup and in-protocol TLS upgrade
- **https**,**http**: port **443**
- **smtps**,**submissions**: port **465**
- **imaps**: port **993**
- **pop3s**: port **995**
- **submission**: port **587** with in-protocol TLS upgrade

## Exported metrics

### Prometheus

Prometheus metrics are available at `http://<prombind>:<promport>/metrics`.

Internal Go metrics are exposed, along with those specific ones:

- `check_errors_total`: total errors checking certificates
- `check_tls_requests_total`: total TLS checks issued
- `check_total`: total checks issued (one of these can trigger multiple TLS
  checks, if host has many A records or MX lookup is done)

then for each TLS check, an entry like so is created, stating how many hours
the certificate will remain valid

- `tls_remaining_hours{protocol=<proto>,requested_hostname=<hostname>,tested_hostname=<host or ip>}`

### Influxdb

InfluxDB metrics will use the provided measurement name and tags, and will add
the following tags:

- `requested_host`: host that has been requested at the CLI
- `checked_host`: host that has been checked in practice
- `host`: hostname executing the check

The following values will be sent:

- `remaining_hours`: how many hours the certificate will remain valid
- `status`: whether the check has been successful or not

## Misc

We love issues & PRs :)

Tests are lacking :'(

## Licence

MIT
