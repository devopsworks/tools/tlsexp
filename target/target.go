package target

import "time"

// Entry holds a requested entry
type Entry struct {
	Requested   string       `json:"requested"`
	Expirations []Expiration `json:"expirations"`
	Protocol    string       `json:"protocol"`
	Port        string       `json:"port"`
}

// Expiration holds an expiration date for a specific entry
type Expiration struct {
	Hostname string    `json:"hostname"`
	IP       string    `json:"ip"`
	SNI      string    `json:"sni"`
	DNSNames []string  `json:"dns_names"`
	Expires  time.Time `json:"expires"`
	Issuer   string    `json:"issuer"`
	Error    error     `json:"error"`
}
