FROM devopsworks/golang-upx:1.22 as builder

ARG version
ARG builddate

WORKDIR /src

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# hadolint ignore=SC2097,SC2098
ENV GOOS=linux \
    GOARCH=amd64 \
    CGO_ENABLED=0

RUN go build \
        -ldflags "-X main.Version=${version} -X main.BuildDate=${builddate}" \
        -a \
    -o /go/bin/tlsexp && \
    strip /go/bin/tlsexp && \
    /usr/local/bin/upx -9 /go/bin/tlsexp

# hadolint ignore=DL3006
FROM gcr.io/distroless/base-debian10

COPY --from=builder /go/bin/tlsexp /usr/local/bin/tlsexp

ENTRYPOINT ["/usr/local/bin/tlsexp"]